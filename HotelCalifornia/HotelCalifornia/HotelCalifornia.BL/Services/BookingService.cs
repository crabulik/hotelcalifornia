﻿using HotelCalifornia.BL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelCalifornia.BL.Entities;
using System.IO;
using HotelCalifornia.BL.DTO;
using HotelCalifornia.BL.Enums;
using HotelCalifornia.BL.Serializers;

namespace HotelCalifornia.BL.Services
{
    public sealed class BookingService : IBookingService
    {
        private const string HotelInfoFile = "hotelInfo.xml";
        private IAppartmentsRepository AppartmentsRepository { get; set; }

        private ICheckInDeskRepository CheckInDeskRepository { get; set; }

        private Hotel _generalHotel;

        public BookingService(IAppartmentsRepository appartmentsRepository, ICheckInDeskRepository checkInDeskRepository)
        {
            AppartmentsRepository = appartmentsRepository;
            CheckInDeskRepository = checkInDeskRepository;

            _generalHotel = new Hotel
            {
                Id = 0,
                Name = "Hotel California",
                Address = "77  Eagles st.",
                Phones = "+380 002-19-77"
            };
        }


        public CheckDeskItem BookAppartment(int appartmentId, DateTime fromDate, DateTime toDate)
        {
            var checkFoBusy = CheckInDeskRepository.GetAllCheckDeskItems(fromDate, toDate).Any(p => p.AppartmentID == appartmentId);
            if (!checkFoBusy)
            {
                var result = new CheckDeskItem
                {
                    AppartmentID = appartmentId,
                    ItemBookingStatus = BookingStatus.Booked,
                    DateBookingBegin = fromDate,
                    DateBookingEnd = toDate
                };
                if (CheckInDeskRepository.Add(result))
                {
                    return result;
                }
            }

            return null;
        }

        public bool MarkCheckDeskItemDeleted(int itemId)
        {
            return CheckInDeskRepository.MarkCheckInItemAsDeleted(itemId);
        }

        public bool AddAppartment(Appartement appartment)
        {
            return AppartmentsRepository.Add(appartment);
        }

        public bool EditAppartmentData(int appartmentId, Appartement newData)
        {
            return AppartmentsRepository.Update(appartmentId, newData);
        }

        public bool DeleteAppartment(int appartmentId)
        {
            if (AppartmentsRepository.Delete(appartmentId))
            {
                CheckInDeskRepository.DeleteForAppartment(appartmentId);
                return true;
            }

            return false;
        }

        public IList<Appartement> GetAllAppartments()
        {
            return AppartmentsRepository.GetAllAppartments();
        }

        public IList<Appartement> GetFreeAppartment(DateTime fromDate, DateTime toDate)
        {
            var busyForPeriod = CheckInDeskRepository.GetAllCheckDeskItems(fromDate, toDate)
                .Where(p => p.ItemBookingStatus != BookingStatus.Deleted).Select(p => p.AppartmentID).ToArray();
            return (from appartments in AppartmentsRepository.GetAllAppartments()
                    where !busyForPeriod.Contains(appartments.Id)
                    select appartments).ToList();

        }

        public IList<DoCheckInDeskItem> GetCheckInDeskForPeriod(DateTime fromDate, DateTime toDate)
        {
            var result = new List<DoCheckInDeskItem>();

            var appartmentList = AppartmentsRepository.GetAllAppartments();
            var checkInRecords = CheckInDeskRepository.GetAllCheckDeskItems(fromDate, toDate).OrderBy(p => p.DateBookingBegin);

            foreach (var item in checkInRecords)
            {

                var displayItem = new DoCheckInDeskItem
                {
                    ItemBookingStatus = item.ItemBookingStatus,
                    DateBookingEnd = item.DateBookingEnd,
                    DateBookingBegin = item.DateBookingBegin,
                    ID = item.ID
                };
                var appartmentForCheckIn = appartmentList.FirstOrDefault(p => p.Id == item.AppartmentID);
                if (appartmentForCheckIn != null)
                {
                    displayItem.AppartmentDescription = appartmentForCheckIn.Description;
                    displayItem.AppartmentNumber = appartmentForCheckIn.Number;
                    displayItem.AppartmentType = appartmentForCheckIn.Type;
                }
                result.Add(displayItem);
            }

            return result;
        }

        public Hotel GetHotelInfo()
        {
            return (Hotel)_generalHotel.Clone();
        }

        public void UpdateHotelInfo(Hotel newData)
        {
            _generalHotel.Id = newData.Id;
            _generalHotel.Name = newData.Name;
            _generalHotel.Address = newData.Address;
            _generalHotel.Phones = newData.Phones;

        }

        public void LoadData()
        {
            var directory = new DirectoryInfo(".");
            var path = Path.Combine(directory.FullName, HotelInfoFile);
            if (File.Exists(path))
            {
                var serializer = new Serializer<Hotel>();
                var list = serializer.LoadData(path);
                if (list.Count > 0)
                {
                    _generalHotel = list[0];
                }
            }

            AppartmentsRepository.LoadData();
            CheckInDeskRepository.LoadData();

        }

        public void SaveData()
        {
            var directory = new DirectoryInfo(".");
            var path = Path.Combine(directory.FullName, HotelInfoFile);
            var serializer = new Serializer<Hotel>();

            var list = new List<Hotel>();
            list.Add(_generalHotel);
            serializer.SaveData(path, list);
            AppartmentsRepository.SaveData();
            CheckInDeskRepository.SaveData();
        }
    }
}
