﻿using HotelCalifornia.BL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelCalifornia.BL.Entities;
using HotelCalifornia.BL.Enums;
using HotelCalifornia.BL.Serializers;

namespace HotelCalifornia.BL.Services
{
    public sealed class CheckInDeskRepository : ICheckInDeskRepository
    {
        private List<CheckDeskItem> _checkedDeskItems; // внутренне хранилище объектов
        private int _maxID = 0;
        private string fileName = "CheckDeskStore.xml";

        /// <summary>
        /// Конструктор
        /// </summary>
        public CheckInDeskRepository()
        {
             _checkedDeskItems = new List<CheckDeskItem>();
        }

        /// <summary>
        /// Добавляет объект бронирования в список
        /// </summary>
        /// <param name="item">Требуемый объкт бронирования</param>
        /// <returns>
        /// true - добавление успешно
        /// false - добавление не произошло
        /// </returns>
        public bool Add(CheckDeskItem item)
        {
            bool res = true;

            // Увеличиваем счетчик для нового объекта
            _maxID += 1;

            try
            {
                item.ID = _maxID;
                _checkedDeskItems.Add(item);
            }
            catch
            {
                // Если добавления не произошло, то счетчик ИД вернуть в исходное
                _maxID -= 1;
                res = false;
            }

            return res;
        }
        
        /// <summary>
        /// Возвращает список сохраненных объектов бронирования в указанном диапазоне дат
        /// </summary>
        /// <param name="from">Граничная дата начала диапазона бронирования</param>
        /// <param name="to">Граничная дата конца диапазона бронирования</param>
        /// <returns>
        /// </returns>
        public List<CheckDeskItem> GetAllCheckDeskItems(DateTime from, DateTime to)
        {
            List<CheckDeskItem> retDeskItems = new List<CheckDeskItem>();

            //foreach (CheckDeskItem curItem in _checkedDeskItems)
            //{
            //    if (!(to < curItem.DateBookingBegin) && !(from > curItem.DateBookingEnd)) // V.V. Variant 1
            //    //if ((from <= curItem.DateBookingBegin && to >= curItem.DateBookingBegin) ||  // V.V. Variant 2
            //    //    (from >= curItem.DateBookingBegin && to <= curItem.DateBookingEnd) ||
            //    //    (from <= curItem.DateBookingEnd && to >= curItem.DateBookingEnd))
            //    {
            //        retDeskItems.Add(curItem);
            //    }
            //}

            // V.V. Variant 3
            //retDeskItems = _checkedDeskItems.FindAll(delegate(CheckDeskItem curItem)
            //                                          {
            //                                              return (curItem.DateBookingBegin < to) && (curItem.DateBookingEnd > from);
            //                                          }
            //);

            // V.V. Variant 4
            retDeskItems =
                _checkedDeskItems.FindAll(curItem => (curItem.DateBookingBegin < to) && (curItem.DateBookingEnd > from));

            return retDeskItems;
        }

        /// <summary>
        /// Возвращает сохраненный объект бронирования с указанным идентификатором
        /// </summary>
        /// <param name="id">Требуемый идентификатор объекта бронирования</param>
        /// <returns></returns>
        public CheckDeskItem GetCheckDeskItem(int id)
        {
            /*foreach(CheckDeskItem curItem in _checkedDeskItems)
            {
                if(curItem.ID == id)
                {
                    return curItem;
                }
            }*/
            _checkedDeskItems.FirstOrDefault(p => p.AppartmentID == id);
            return null;
        }

        public bool Update(int id, CheckDeskItem newData)
        {
            bool result = false;
            var editItem = GetCheckDeskItem(id);
            if(editItem != null)
            {
                editItem.DateBookingBegin = newData.DateBookingBegin;
                editItem.DateBookingEnd = newData.DateBookingEnd;
                editItem.ItemBookingStatus = newData.ItemBookingStatus;

                result = true;
            }

            return result;
        }

        public bool DeleteForAppartment(int appartmentId)
        {
            bool result = false;

            for (int i = 0; i < _checkedDeskItems.Count; i++)
            {
                if (_checkedDeskItems[i].AppartmentID == appartmentId)
                {
                    _checkedDeskItems.RemoveAt(i);

                    i -= 1;

                    result = true;
                }
            }

            return result;
        }

        public bool MarkCheckInItemAsDeleted(int itemId)
        {
            bool result = false;

            var editItem = GetCheckDeskItem(itemId);
            if (editItem != null)
            {
                editItem.ItemBookingStatus = BookingStatus.Deleted;

                result = true;
            }

            return result;
        }

        public void LoadData()
        {
            IList<CheckDeskItem> loadedData = new Serializer<CheckDeskItem>().LoadData(fileName);
            if (loadedData != null)
            {
                _checkedDeskItems = (List< CheckDeskItem>)loadedData;

                FindMaxID();
            }

        }

        public void SaveData()
        {
            Serializer<CheckDeskItem> serializer = new Serializer<CheckDeskItem>();
            serializer.SaveData(fileName, _checkedDeskItems);
        }

        private void FindMaxID()
        {
            _maxID = 0;

            if(_checkedDeskItems.Count > 0)
            {
                _maxID = _checkedDeskItems.Max(p => p.AppartmentID);
            }

            /*
            foreach (CheckDeskItem curItem in _checkedDeskItems)
            {
                if (curItem.ID > res)
                {
                    res = curItem.ID;
                }
            }

            _maxID = res;
            */
        }

    }
}
