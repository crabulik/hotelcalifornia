﻿using HotelCalifornia.BL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelCalifornia.BL.Entities;
using HotelCalifornia.BL.Serializers;

namespace HotelCalifornia.BL.Services
{
    public sealed class ApparmentRepository : IAppartmentsRepository
    {
        private IList<Appartement> _list;
        private const string Filename = "Appartements.xml";
        private int _maxApartmentId;

        public ApparmentRepository()
        {
            _list = new List<Appartement>();
            _list.Add(new Appartement { Id = 1, Type = Enums.AppartementType.Economy, Number = "1", Description = "Economy appartement" });
            _list.Add(new Appartement { Id = 2, Type = Enums.AppartementType.Standart, Number = "2", Description = "Standart appartement" });
            _list.Add(new Appartement { Id = 3, Type = Enums.AppartementType.Standart, Number = "3", Description = "Standart appartement" });
            _list.Add(new Appartement { Id = 4, Type = Enums.AppartementType.Standart, Number = "4", Description = "Standart appartement" });
            _list.Add(new Appartement { Id = 5, Type = Enums.AppartementType.Lux, Number = "5", Description = "Lux appartement" });

            _maxApartmentId = 5;
        }

        public IList<Appartement> GetAllAppartments()
        {
            return _list;
        }

        public Appartement GetAppartmentById(int Id)
        {
            if (_list != null)
            {
                /*
                foreach (Appartement app in _list)
                {
                    if (app.Id == Id)
                        return app;
                }
                */
                return _list.FirstOrDefault(p => p.Id == Id);

            }
            return null;
        }

        public bool Add(Appartement item)
        {
            var result = true;
            _maxApartmentId += 1;

            try
            {
                item.Id = _maxApartmentId;
                _list.Add(item);
            }
            catch
            {
                _maxApartmentId -= 1;
                result = false;
            }
            return result;
        }

        public bool Update(int id, Appartement newData)
        {
            var result = true;

            try
            {
                var oldApartmentData = GetAppartmentById(id);
                oldApartmentData.Number = newData.Number;
                oldApartmentData.Type = newData.Type;
                oldApartmentData.Description = newData.Description;
            }
            catch (Exception )
            {
                //e.Message;   log what's happened
                result = false;
            }
            return result;
        }

        public bool Delete(int id)
        {
            var result = true;
            try
            {
                var item = _list.FirstOrDefault(apartment => apartment.Id == id);
                if(item != null)
                    _list.Remove(item);
            }
            catch (Exception)
            {
                
                //e.Message;   log what's happened
                result = false;
            }
            return result;
        }

        public void LoadData()
        {
            IList<Appartement> data = new Serializer<Appartement>().LoadData(Filename);
            if (data != null)
            {
                _list = data;
                FindMaxApartmentId();
            }            
        }

        public void SaveData()
        {
            new Serializer<Appartement>().SaveData(Filename, _list);
        }

        private void FindMaxApartmentId()
        {
            /*
            var result = 0;

            foreach (var item in _list)
            {
                if (item.Id > result)
                {
                    result = item.Id;
                }
            }
            
            _maxApartmentId = result;
            */
            _maxApartmentId = 0;
            if (_list.Count > 0)
            {
                _maxApartmentId = _list.Max(p => p.Id);
            }
        }

    }
}
