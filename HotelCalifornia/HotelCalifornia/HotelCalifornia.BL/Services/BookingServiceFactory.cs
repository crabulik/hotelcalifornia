﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Services
{
    public sealed class BookingServiceFactory
    {
        public BookingService GetService()
        {
            var appartmentsRepository = new ApparmentRepository();
            var checkInDeskRepository = new CheckInDeskRepository();

            var mainService = new BookingService(appartmentsRepository,
                checkInDeskRepository);

            return mainService;
        }
    }
}
