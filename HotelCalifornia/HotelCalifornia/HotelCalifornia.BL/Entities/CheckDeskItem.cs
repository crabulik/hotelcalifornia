﻿using HotelCalifornia.BL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Entities
{
    [Serializable]
    public class CheckDeskItem
    {
        private int id;
        private int appartmentID;
        private DateTime dateBookingBegin;
        private DateTime dateBookingEnd;
        private BookingStatus bookingStatus;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public int AppartmentID
        {
            get { return appartmentID; }
            set { appartmentID = value; }
        }

        public DateTime DateBookingBegin
        {
            get { return dateBookingBegin; }
            set { dateBookingBegin = value; }
        }

        public DateTime DateBookingEnd
        {
            get { return dateBookingEnd; }
            set { dateBookingEnd = value; }
        }

        public BookingStatus ItemBookingStatus
        {
            get { return bookingStatus; }
            set { bookingStatus = value; }
        }

        public override string ToString()
        {
            return String.Format("CheckIn ID: {0}; AppartmentID: \"{1}\"; \nBooking begin: {2}; \nBooking end: {3} \nBooking status: {4}"
                                ,
                                id.ToString(), 
                                appartmentID.ToString(), 
                                dateBookingBegin.ToShortDateString(), 
                                dateBookingEnd.ToShortDateString(), 
                                bookingStatus.ToString());
        }
    }
}
