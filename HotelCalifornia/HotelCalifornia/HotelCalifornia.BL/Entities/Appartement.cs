﻿using HotelCalifornia.BL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Entities
{
    [Serializable]
    public class Appartement: ICloneable
    {
        
        private int _appartementId;
        private string _appartementNumber;
        private string _appartementDescription;
        private AppartementType _appartementType;

       

            public int Id
        {
            get { return _appartementId; }
            set { _appartementId = value; }            
        }

        public string Number
        {
            get { return _appartementNumber; }
            set { _appartementNumber = value; }
        }

        public string Description
        {
            get { return _appartementDescription; }
            set { _appartementDescription = value; }
        }

        public AppartementType Type
        {
            get { return _appartementType; }
            set { _appartementType = value; }
        }

        public override string ToString()
        {
            return String.Format("Апартаменты №: {0}, Класс: {1}, Описание: {2}",
                _appartementNumber, 
                _appartementType, 
                _appartementDescription);
        }

        public object Clone()
        {
            var result = new Appartement();
            result.Id = Id;
            result.Description = Description;
            result.Type = Type;
            result.Number = Number;

            return result;
        }
    }
}
