﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Entities
{
    [Serializable]
    public class Hotel: ICloneable 
        {

        private int _hotelID;
        private String _hotelName;
        private String _hotelAddress;
        private String _hotelPhones;

        public int Id
        {
            get { return _hotelID; }
            set { _hotelID = value; }
        }

        public string Name {
            get { return _hotelName; }
            set { _hotelName = value; }
        }

        public string Address
        {
            get { return _hotelAddress; }
            set { _hotelAddress = value; }
        }

        public string Phones
        {
            get { return _hotelPhones; }
            set { _hotelPhones = value; }
        }

        public override string ToString()
        {
            return String.Format("Hotel ID: {0}; Name: \"{1}\"; \nAddress: {2}; \nPhones: {3}", Id, Name, Address, Phones);
        }

        public object Clone()
        {
            var result = new Hotel();
            result.Id = Id;
            result.Name = Name;
            result.Address = Address;
            result.Phones = Phones;
            return result;
        }
    }
}
