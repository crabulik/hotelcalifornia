﻿using HotelCalifornia.BL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Contracts
{
    public interface ICheckInDeskRepository
    {
        List<CheckDeskItem> GetAllCheckDeskItems(DateTime from, DateTime to);

        CheckDeskItem GetCheckDeskItem(int id);

        bool Add(CheckDeskItem item);

        bool Update(int id, CheckDeskItem newData);

        bool DeleteForAppartment(int appartmentId);

        bool MarkCheckInItemAsDeleted(int itemId);

        void SaveData();

        void LoadData();
    }
}
