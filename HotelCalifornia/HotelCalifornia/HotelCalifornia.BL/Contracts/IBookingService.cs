﻿using HotelCalifornia.BL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Contracts
{
    public interface IBookingService
    {
        Hotel GetHotelInfo();

        void UpdateHotelInfo(Hotel newData);

        IList<Appartement> GetAllAppartments();

        IList<Appartement> GetFreeAppartment(DateTime fromDate, DateTime toDate);

        CheckDeskItem BookAppartment(int appartmentId, DateTime fromDate, DateTime toDate);

        bool MarkCheckDeskItemDeleted(int itemId);


        bool AddAppartment(Appartement appartment);

        bool EditAppartmentData(int appartmentId, Appartement newData);

        bool DeleteAppartment(int appartmentId);

        
    }
}
