﻿using HotelCalifornia.BL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Contracts
{
    public interface IAppartmentsRepository
    {
        IList<Appartement> GetAllAppartments();

        Appartement GetAppartmentById(int Id);

        bool Add(Appartement item);

        bool Update(int id, Appartement newData);

        bool Delete(int id);

        void SaveData();

        void LoadData();
    }
}
