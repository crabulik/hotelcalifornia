﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace HotelCalifornia.BL.Serializers
{
    public class Serializer<T>
    {

        public void SaveData(string fileName, IList<T> list)
        {
            var directory = new DirectoryInfo(".");
            var path = Path.Combine(directory.FullName, fileName);
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                //var formater = new XmlSerializer(typeof(T));  //  V.V. Так НЕ работает
                var formater = new XmlSerializer(list.GetType()); //  V.V. Так работает
                formater.Serialize(stream, list);
            }
        }

        public IList<T> LoadData(string fileName)
        {
            List<T> temp = new List<T>(); // V.V. временная переменная для правильной инициализации сериализатора

            var directory = new DirectoryInfo(".");
            var path = Path.Combine(directory.FullName, fileName);

            if (!File.Exists(path)) return default(IList<T>);

            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                //var formater = new XmlSerializer(typeof(T));  //  V.V. Так НЕ работает
                var formater = new XmlSerializer(temp.GetType()); //  V.V. Так работает
                var result = formater.Deserialize(stream);
                if (result == null)
                {
                    return default(IList<T>);
                }
                return (IList<T>)result;
            }
        }
    }
}
