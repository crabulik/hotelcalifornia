﻿using System;
using HotelCalifornia.BL.Enums;

namespace HotelCalifornia.BL.DTO
{
    //P.S.: Подобные дата трансфер объеты используются для передачи данных например на окно в удобном для вывода виде,
    //      НЕ являются Бизнес Объектами.
    public class DoCheckInDeskItem
    {
        public int ID { get; set; }

        public DateTime DateBookingEnd { get; set; }

        public DateTime DateBookingBegin { get; set; }

        public BookingStatus ItemBookingStatus { get; set; }

        public string AppartmentNumber { get; set; }

        public string AppartmentDescription { get; set; }

        public AppartementType AppartmentType { get; set; }

        public override string ToString()
        {
            return $"ID: {ID}, DateBookingEnd: {DateBookingEnd}, ItemBookingStatus: {ItemBookingStatus}, DateBookingBegin: {DateBookingBegin}, AppartmentNumber: {AppartmentNumber}, AppartmentDescription: {AppartmentDescription}, AppartmentType: {AppartmentType}";
        }
    }
}