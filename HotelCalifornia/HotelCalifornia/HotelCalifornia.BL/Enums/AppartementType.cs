﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Enums
{
    public enum AppartementType
    {
        Economy = 10,
        Standart = 20,
        Lux = 30    
    }
}
