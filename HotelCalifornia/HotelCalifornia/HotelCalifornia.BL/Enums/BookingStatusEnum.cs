﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Enums
{
    public enum BookingStatus
    {
        Booked = 0,
        Free = 1,
        Deleted = 2
    }
}
