﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCalifornia.BL.Enums
{
    public enum SaveOperationEnum : Int32
    {
        Save = 1,
        Load = 2,
        Cancel = 3
    }
}
