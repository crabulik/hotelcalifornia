﻿using System;
using HotelCalifornia.BL.Enums;

namespace HotelCalifornia
{
    public static class AppartmentConverter
    {
        public static string AppartementTypeToString(AppartementType value)
        {
            switch (value)
            {
                case AppartementType.Economy:
                    return ManagerResorces.rs_AppartementTypeEconomy;
                case AppartementType.Standart:
                    return ManagerResorces.rs_AppartementTypeStandart;
                case AppartementType.Lux:
                    return ManagerResorces.rs_AppartementTypeLux;
            }

            return String.Empty;
        }

        public static string BookingStatusToString(BookingStatus value)
        {
            switch (value)
            {
                case BookingStatus.Booked:
                    return ManagerResorces.rs_BookingStatusBooked;
                case BookingStatus.Deleted:
                    return ManagerResorces.rs_BookingStatusDeleted;
                case BookingStatus.Free:
                    return ManagerResorces.rs_BookingStatusFree;
            }

            return String.Empty;
        }
    }
}