﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelCalifornia.BL.Entities;

namespace HotelCalifornia.Comparators
{
    class ApartmentsComparator
    {
        public static IList<Appartement> SortApartmentsByNumberAsc(IList<Appartement> list)
        {
            return list.OrderBy(p => p, new ApartmentNumberComparer()).ToList();
        }

        public static IList<Appartement> SortApartmentsByNumberDesc(IList<Appartement> list)
        {
            return list.OrderByDescending(p => p, new ApartmentNumberComparer()).ToList();
        }

        public static IList<Appartement> SortApartmentsByTypeAsc(IList<Appartement> list)
        {
            return list.OrderBy(p => p, new ApartmentTypeComparer()).ToList();
        }

        public static IList<Appartement> SortApartmentsByTypeDesc(IList<Appartement> list)
        {
            return list.OrderByDescending(p => p, new ApartmentTypeComparer()).ToList();
        }
    }
}
