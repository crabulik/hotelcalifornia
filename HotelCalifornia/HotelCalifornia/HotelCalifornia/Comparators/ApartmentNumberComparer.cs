﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelCalifornia.BL.Entities;

namespace HotelCalifornia.Comparators
{
    class ApartmentNumberComparer : Comparer<Appartement>
    {
        public override int Compare(Appartement x, Appartement y)
        {
            if (x == null || y == null)
            {
                throw new ArgumentNullException("null arguments!!!");
            }
            return String.Compare(x.Number, y.Number, StringComparison.Ordinal);
        }
    }
}
