﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelCalifornia.BL.Entities;

namespace HotelCalifornia.Sort_classes
{
    public class CheckDeskItemComparerDateBookingBeginOnly : Comparer<CheckDeskItem>
    {
        public override int Compare(CheckDeskItem x, CheckDeskItem y)
        {
            if (x == null || y == null)
            {
                throw new ArgumentNullException("CheckDeskItem can't be a null!");
            }

            return x.DateBookingBegin.CompareTo(y.DateBookingBegin);
        }
    }

}
