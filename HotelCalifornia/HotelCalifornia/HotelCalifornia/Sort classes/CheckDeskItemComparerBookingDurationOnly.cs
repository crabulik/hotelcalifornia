﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelCalifornia.BL.Entities;

namespace HotelCalifornia.Sort_classes
{
    public class CheckDeskItemComparerBookingDurationOnly : Comparer<CheckDeskItem>
    {
        public override int Compare(CheckDeskItem x, CheckDeskItem y)
        {
            if (x == null || y == null)
            {
                throw new ArgumentNullException("CheckDeskItem can't be a null!");
            }

            var xSpan = x.DateBookingEnd.Subtract(x.DateBookingBegin);
            var ySpan = y.DateBookingEnd.Subtract(y.DateBookingBegin);

            return xSpan.CompareTo(ySpan);
        }
    }
}
