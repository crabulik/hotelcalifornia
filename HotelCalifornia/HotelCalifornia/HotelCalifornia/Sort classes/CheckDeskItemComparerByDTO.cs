﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelCalifornia.BL.DTO;
using HotelCalifornia.BL.Entities;

namespace HotelCalifornia.Sort_classes
{
    public class CheckDeskItemComparerByDTO : Comparer<DoCheckInDeskItem>
    {
        public override int Compare(DoCheckInDeskItem x, DoCheckInDeskItem y)
        {
            if (x == null || y == null)
            {
                throw new ArgumentNullException("CheckDeskItem can't be a null!");
            }

            var xSpan = x.DateBookingEnd.Subtract(x.DateBookingBegin);
            var ySpan = y.DateBookingEnd.Subtract(y.DateBookingBegin);

            if (String.Compare(x.AppartmentNumber, y.AppartmentNumber, StringComparison.InvariantCultureIgnoreCase) != 0)
            {
                return String.Compare(x.AppartmentNumber, y.AppartmentNumber, StringComparison.InvariantCultureIgnoreCase);
            }
            else if (xSpan.CompareTo(ySpan) != 0)
            {
                return xSpan.CompareTo(ySpan);
            }
            else if (x.AppartmentType.CompareTo(y.AppartmentType) != 0)
            {
                // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                return x.AppartmentType.CompareTo(y.AppartmentType);
            }
            else
            {
                return 0;
            }
        }
    }
}
