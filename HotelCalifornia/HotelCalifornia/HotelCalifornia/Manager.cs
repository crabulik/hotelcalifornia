﻿using HotelCalifornia.BL.Entities;
using HotelCalifornia.BL.Services;
using System;
using System.Collections.Generic;
using ConsoleCommon;
using HotelCalifornia.BL.DTO;
using HotelCalifornia.BL.Enums;
using System.Globalization;
using System.Text;
using System.Linq;
using HotelCalifornia.Sort_classes;
using HotelCalifornia.Comparators;

namespace HotelCalifornia
{
    
    public partial class Manager
    {
        private const string CheckYesAnswer = "y";
        private string current;
        private BookingService _bookingService;
        private const string AdminDefaultLogin = "admin";
        private const string AdminDefaultPass = "admin";
        private const string ruLocale = "ru-RU";


        private string AppartmentToString(Appartement appartement)
        {
            return string.Format("{0}: {1}, {2}: {3}, {4}: {5}", ManagerResorces.rs_ApartmentNumber, appartement.Number,
                ManagerResorces.rs_ApartmentType, AppartmentConverter.AppartementTypeToString(appartement.Type),
                ManagerResorces.rs_ApartmentDescription, appartement.Description);
        }

        protected void WriteOnConsoleAppartmentsList(IList<Appartement> list)
        {
            foreach (var apartment in list)
            {
                Console.WriteLine(AppartmentToString(apartment));
            }
        }

        protected void ShowAllAppartmentsCommand()
        {
            Console.WriteLine(ManagerResorces.rs_ManagerAllAppartmentsCaption);
            
            WriteOnConsoleAppartmentsList(_bookingService.GetAllAppartments());           
        }

        protected void ShowAllFreeAppsForDateCommand()
        {
            DateTime periodBegin, periodEnd;
            string dateInput;
            List<Appartement> freeAppartmentsByDate = new List<Appartement>();

            try
            {
               // Console.WriteLine("{0}\n", ManagerResorces.rs_DataTime);

                Console.WriteLine(ManagerResorces.rs_Booking_DateBegin);
                periodBegin = ConsoleFunnyFunctions.ReadPersistentDate(ManagerResorces.rs_DataTime, ManagerResorces.rs_ErrodDateFormate, ruLocale);

                Console.WriteLine(ManagerResorces.rs_Booking_DateEnd);
                periodEnd = ConsoleFunnyFunctions.ReadPersistentDate(ManagerResorces.rs_DataTime, ManagerResorces.rs_ErrodDateFormate, ruLocale);

                freeAppartmentsByDate = (List<Appartement>)_bookingService.GetFreeAppartment(periodBegin, periodEnd);
                WriteOnConsoleAppartmentsList(freeAppartmentsByDate);

                Console.WriteLine("{0}\n", ManagerResorces.rs_Book);
                var selectBook = Console.ReadLine();
                if (String.Equals(selectBook, CheckYesAnswer, StringComparison.CurrentCultureIgnoreCase))
                {
                    //BookingApartmentCommand_ApartmentNumber(freeAppartmentsByDate, periodBegin, periodEnd);
                    BookingApartmentCommand_MenuNumber(freeAppartmentsByDate, periodBegin, periodEnd);
                }
                else
                {
                    Console.WriteLine(ManagerResorces.rs_SelectDisplay);
                    current = Console.ReadLine();
                }


            }
            catch (Exception)
            {
                Console.WriteLine("{0}\n{1}", ManagerResorces.rs_InvalidInput, ManagerResorces.rs_SelectDisplay);
                current = Console.ReadLine();
            }
        }

        private void BookingApartmentCommand_ApartmentNumber(List<Appartement> freeAppartmentsByDate, DateTime bookingPeriodBegin, DateTime bookingPeriodEnd)
        {
            Int32 apartmentNumber = -1;
            bool checkResult = false;

            Console.WriteLine(ManagerResorces.rs_RoomNumber);

            while (true)
            {
                apartmentNumber = ConsoleFunnyFunctions.GetPersistentInt(String.Empty,
                                                                         ManagerResorces.rs_VariantOutOfBounds,
                                                                         ManagerResorces.rs_VariantIsEmptyError);

                checkResult = CheckApartmentNumberIsCorrectForBooking(apartmentNumber, freeAppartmentsByDate);
                if (checkResult == false)
                {
                    Console.WriteLine(ManagerResorces.rs_Booking_ApartmentNumberIsNotCorrect);

                    Console.WriteLine("{0}\n", ManagerResorces.rs_Booking_QuestionCalcelBooking);
                    var userChoice = Console.ReadLine();
                    if (String.Equals(userChoice, CheckYesAnswer, StringComparison.CurrentCultureIgnoreCase))
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                Appartement targetApartment = freeAppartmentsByDate.Find(
                                                                delegate (Appartement apartment)
                                                                {
                                                                    return apartment.Number.Equals(apartmentNumber.ToString());
                                                                }
                                                                );
                if (targetApartment != null)
                {
                    _bookingService.BookAppartment(targetApartment.Id, bookingPeriodBegin, bookingPeriodEnd);
                    Console.WriteLine(ManagerResorces.rs_Booking_BookingDone);
                    break;
                }
                else
                {
                    break;
                }
            }
        }

        private void BookingApartmentCommand_MenuNumber(List<Appartement> freeAppartmentsByDate, DateTime bookingPeriodBegin, DateTime bookingPeriodEnd)
        {
            int cancelMenuItem = -1;

            Console.WriteLine(ManagerResorces.rs_Booking_SelectApartmentNumber);
            Dictionary<int, Appartement> appDictionary = GetAppartmentDictionary(freeAppartmentsByDate);

            cancelMenuItem = appDictionary.Count + 1;

            foreach (var item in appDictionary)
            {
                Console.WriteLine("{0}: {1}", item.Key, AppartmentToString(item.Value));
            }
            Console.WriteLine("{0}: {1}", cancelMenuItem, ManagerResorces.rs_Booking_CalcelBooking);

            int selectNumber = ConsoleFunnyFunctions.GetPersistentVarian(1, appDictionary.Count + 1, "",
                    ManagerResorces.rs_IncorectVariantData, ManagerResorces.rs_VariantIsEmptyError,
                    ManagerResorces.rs_VariantOutOfBounds);

            if (selectNumber != cancelMenuItem)
            {
                _bookingService.BookAppartment(appDictionary[selectNumber].Id, bookingPeriodBegin, bookingPeriodEnd);
                Console.WriteLine(ManagerResorces.rs_Booking_BookingDone);
            }
        }

        private bool CheckApartmentNumberIsCorrectForBooking(int roomNumber, List<Appartement> freeAppartmentsByDate)
        {
            bool result = false;

            foreach(Appartement curApartment in freeAppartmentsByDate)
            {
                if (Convert.ToInt32(curApartment.Number) == roomNumber)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        protected void ShowCheckInDesckForPeriod()
        {
            DateTime periodBegin, periodEnd;
            
            Console.WriteLine(ManagerResorces.rs_Booking_DateBegin);
            periodBegin = ConsoleFunnyFunctions.ReadPersistentDate(ManagerResorces.rs_DataTime, ManagerResorces.rs_ErrodDateFormate, ruLocale);

            Console.WriteLine(ManagerResorces.rs_Booking_DateEnd);
            periodEnd = ConsoleFunnyFunctions.ReadPersistentDate(ManagerResorces.rs_DataTime, ManagerResorces.rs_ErrodDateFormate, ruLocale);

            var checkInDesck = _bookingService.GetCheckInDeskForPeriod(periodBegin, periodEnd);

            Console.WriteLine(ManagerResorces.rs_CheckInDeskHeader, periodBegin.ToShortDateString(), periodEnd.ToShortDateString());
            foreach (var item in checkInDesck)
            {
                Console.WriteLine(DoCheckInDeskItemToString(item));
            }
            Console.WriteLine(ManagerResorces.rs_ManyLines);

            var checkInDesckSorted = SortCheckinDeskByDtoBookingDurationOnly(checkInDesck);
            Console.WriteLine(ManagerResorces.rs_CheckInDeskHeader, periodBegin.ToShortDateString(), periodEnd.ToShortDateString());
            foreach (var item in checkInDesckSorted)
            {
                Console.WriteLine(DoCheckInDeskItemToString(item));
            }
            Console.WriteLine(ManagerResorces.rs_ManyLines);

            var checkInDesckSorted1 = SortCheckinDeskByDtoMain(checkInDesck);
            Console.WriteLine(ManagerResorces.rs_CheckInDeskHeader, periodBegin.ToShortDateString(), periodEnd.ToShortDateString());
            foreach (var item in checkInDesckSorted1)
            {
                Console.WriteLine(DoCheckInDeskItemToString(item));
            }
            Console.WriteLine(ManagerResorces.rs_ManyLines);
        }

        private string DoCheckInDeskItemToString(DoCheckInDeskItem item)
        {
            return string.Format("{6} - {7} || {0}: {1}, {2}: {3}, {4}: {5}, {8}: {9}", ManagerResorces.rs_ApartmentNumber, item.AppartmentNumber,
                ManagerResorces.rs_ApartmentType, AppartmentConverter.AppartementTypeToString(item.AppartmentType),
                ManagerResorces.rs_ApartmentDescription, item.AppartmentDescription,
                item.DateBookingBegin.ToShortDateString(), item.DateBookingEnd.ToShortDateString(),
                ManagerResorces.rs_BookingStatus, AppartmentConverter.BookingStatusToString(item.ItemBookingStatus));
        }

        private IList<DoCheckInDeskItem> SortCheckinDeskByDtoMain(IList<DoCheckInDeskItem> checkinDeskItems)
        {
            try
            {

                return checkinDeskItems.OrderBy(p => p, new CheckDeskItemComparerByDTO()).ToList();
            }
            catch (Exception exc)
            {
                return null;
            }
        }

        private IList<DoCheckInDeskItem> SortCheckinDeskByDtoBookingDurationOnly(IList<DoCheckInDeskItem> checkinDeskItems)
        {
            try
            {

                return checkinDeskItems.OrderBy(p => p, new CheckDeskItemComparerByDtoBookingDurationOnly()).ToList();
            }
            catch (Exception exc)
            {
                return null;
            }
        }

        protected void SaveDataCommand()
        {
            int userChoice = -1;
            bool checkResult = false;

            ShowSaveLoadSubmenu();

            do
            {
                userChoice = ConsoleFunnyFunctions.GetPersistentInt(ManagerResorces.rs_InputValue_Int + " ",
                    ManagerResorces.rs_VariantOutOfBounds,
                    ManagerResorces.rs_VariantIsEmptyError);

                checkResult = CheckOperationCode(userChoice);
                if (checkResult == false)
                {
                    Console.WriteLine(ManagerResorces.rs_VariantOutOfBounds);
                }
            }
            while (checkResult == false);

            switch (userChoice)
            {
                case (int) SaveOperationEnum.Save:
                    try
                    {
                        _bookingService.SaveData();
                        Console.WriteLine(ManagerResorces.rs_SaveOperation_Done);
                    }
                    catch(Exception exc)
                    {
                        Console.WriteLine(ManagerResorces.rs_SaveOperation_Fail + " : " + exc.Message);
                    }
                    break;
                case (int) SaveOperationEnum.Load:
                    try
                    {
                        _bookingService.LoadData();
                        Console.WriteLine(ManagerResorces.rs_LoadOperation_Done);
                    }
                    catch(Exception exc)
                    {
                        Console.WriteLine(ManagerResorces.rs_SaveOperation_Fail + " : " + exc.Message);
                    }
                    break;
                case (int)SaveOperationEnum.Cancel:
                    Console.WriteLine(ManagerResorces.rs_SaveMenu_Abort);
                    break;
            }
            
        }

        private bool CheckOperationCode(int userInput)
        {
            bool result = true;

            if (userInput != (int)SaveOperationEnum.Save &&
                userInput != (int)SaveOperationEnum.Load &&
                userInput != (int)SaveOperationEnum.Cancel)
            {
                result = false;
            }

            return result;
        }

        private void ShowSaveLoadSubmenu()
        {
            Console.WriteLine(String.Format(ManagerResorces.rs_SaveMenu_WelcomeMessage + Environment.NewLine + "{0} : {1}" + Environment.NewLine + "{2} -> {3}" + Environment.NewLine + "{4} -> {5}" + Environment.NewLine,
                                            ManagerResorces.rs_SaveMenu_Save,
                                            (int)SaveOperationEnum.Save,
                                            ManagerResorces.rs_SaveMenu_Load,
                                            (int)SaveOperationEnum.Load,
                                            ManagerResorces.rs_SaveMenu_Abort,
                                            (int)SaveOperationEnum.Cancel));
        }

        private void ManagerMenueCommandCommand()
        {
            Console.WriteLine(ManagerResorces.rs_Login);
            var login = (Console.ReadLine());
            Console.WriteLine(ManagerResorces.rs_Password);
            var password = (Console.ReadLine());
            if ((string.Equals(password, AdminDefaultPass)) && (string.Equals(login, AdminDefaultLogin)))
            {

                while (true)
                {
                    Console.WriteLine(ManagerResorces.rs_ManagerMenu);
                    int selectMenu = ConsoleFunnyFunctions.GetPersistentVarian(1, 6, "",
                        ManagerResorces.rs_IncorectVariantData, ManagerResorces.rs_VariantIsEmptyError,
                        ManagerResorces.rs_VariantOutOfBounds);

                    switch (selectMenu)
                    {
                        case 1:
                            ShowAllAppartmentsCommand();
                            break;
                        case 2:
                            AddAppartmentCommand();
                            break;
                        case 3:
                            EditAppartmentCommand();
                            break;
                        case 4:
                            DeleteAppartmentCommand();
                            break;
                        case 5:
                            //Console.WriteLine("test version");
                            EditHotelInfoCommand();
                            break;
                        case 6:
                            return;
                    }
                }
            }
            else
            {
                Console.WriteLine(ManagerResorces.rs_InvalidInput);
            }
            
        }

        #region Manager Commands

        private void AddAppartmentCommand()
        {
            Console.WriteLine(ManagerResorces.rs_InputAppartmentInfoHeader);
            var result = FillAppartementData();
            if (result != null)
                if (_bookingService.AddAppartment(result))
                {
                    Console.WriteLine(ManagerResorces.rs_AppartmentAddSuccess);
                }
        }

        private void EditAppartmentCommand()
        {
            Console.WriteLine(ManagerResorces.rs_SelectAppartmentToEdit);
            var appDictionary = GetAppartmentDictionary();
            foreach (var item in appDictionary)
            {
                Console.WriteLine("{0}: {1}", item.Key, AppartmentToString(item.Value));
            }
            int selectNumber = ConsoleFunnyFunctions.GetPersistentVarian(1, appDictionary.Count + 1, "",
                    ManagerResorces.rs_IncorectVariantData, ManagerResorces.rs_VariantIsEmptyError,
                    ManagerResorces.rs_VariantOutOfBounds);
            var selectedAppartment = appDictionary[selectNumber];

            var editedData = EditAppartmentData(selectedAppartment);

            _bookingService.EditAppartmentData(selectedAppartment.Id, editedData);
        }

        private Appartement EditAppartmentData(Appartement old)
        {
            var result = (Appartement) old.Clone();
            Console.WriteLine(ManagerResorces.rs_SelectedAppartmentInfo);
            Console.WriteLine(AppartmentToString(old));


            Console.Write(ManagerResorces.rs_EditAppartmentNumber);
            var input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                result.Number = input;
            }

            Console.WriteLine(ManagerResorces.rs_EditAppartmentTypeQuestion);
            var respond = Console.ReadLine();
            if (!(string.IsNullOrEmpty(respond)) && string.Equals(respond, CheckYesAnswer, StringComparison.CurrentCultureIgnoreCase))
            {
                result.Type = GetAppartementType();
            }

            Console.Write(ManagerResorces.rs_EditAppartmentDescription);
            input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                result.Description = input;
            }

            return result;
        }

        private void DeleteAppartmentCommand()
        {
            Console.WriteLine(ManagerResorces.rs_SelectAppartmentToDelete);
            var appDictionary = GetAppartmentDictionary();
            foreach (var item in appDictionary)
            {
                Console.WriteLine("{0}: {1}", item.Key, AppartmentToString(item.Value));
            }
            int selectNumber = ConsoleFunnyFunctions.GetPersistentVarian(1, appDictionary.Count+1, "",
                    ManagerResorces.rs_IncorectVariantData, ManagerResorces.rs_VariantIsEmptyError,
                    ManagerResorces.rs_VariantOutOfBounds);

            if (_bookingService.DeleteAppartment(appDictionary[selectNumber].Id))
            {
                Console.WriteLine(ManagerResorces.rs_AppartmentDeleteSuccess);
            }
        }

        private void EditHotelInfoCommand()
        {
            Console.WriteLine(ManagerResorces.rs_ManagerMenuEditing);
            int selectMenu = ConsoleFunnyFunctions.GetPersistentVarian(1, 2, "",
                    ManagerResorces.rs_IncorectVariantData, ManagerResorces.rs_VariantIsEmptyError,
                    ManagerResorces.rs_VariantOutOfBounds);

            switch (selectMenu)
            {
                case 1 :
                    WriteHotelInfo();
                    break;
                case 2 :
                    EditHotelInfoComman();
                    break;
                
            }
            
            


    }

        private void EditHotelInfoComman()
        {
            var result = new Hotel();
            Console.WriteLine(ManagerResorces.rs_EditHotelid);
            result.Id = int.Parse(Console.ReadLine());
            Console.WriteLine(ManagerResorces.rs_EditHotelName);
            result.Name = Console.ReadLine();
            Console.WriteLine(ManagerResorces.rs_EditHotelAddress);
            result.Address = Console.ReadLine();
            Console.WriteLine(ManagerResorces.rs_EditHotelPhone);
            result.Phones = Console.ReadLine();
            Console.WriteLine(ManagerResorces.rs_AppartmentAddSuccess);

            _bookingService.UpdateHotelInfo(result);

            Console.WriteLine(ManagerResorces.rs_HotelInfoSuccessChanged);
        }

        private string HotelToString(Hotel hotel)
        {
            var sb = new StringBuilder(ManagerResorces.rs_HotelNameCaption);
            sb.AppendLine(hotel.Name);

            sb.Append(ManagerResorces.rs_HotelAddressesCaption);
            sb.AppendLine(hotel.Address);

            sb.Append(ManagerResorces.rs_HotelPhonesCaption);
            sb.AppendLine(hotel.Phones);
            return sb.ToString();
        }

        private void WriteHotelInfo()
        {
           
            Console.WriteLine(HotelToString(_bookingService.GetHotelInfo()));
           
        }
        
        private Dictionary<int, Appartement> GetAppartmentDictionary()
        {
            var dictionary = new Dictionary<int, Appartement>();

            var values = _bookingService.GetAllAppartments();
            for (int i = 0; i < values.Count; i++)
            {
                var value = values[i];
                dictionary.Add(i + 1, values[i]);
            }

            return dictionary;
        }

        private Dictionary<int, Appartement> GetAppartmentDictionary(List<Appartement> selectedAppartments)
        {
            var dictionary = new Dictionary<int, Appartement>();
            
            for (int i = 0; i < selectedAppartments.Count; i++)
            {
                var value = selectedAppartments[i];
                dictionary.Add(i + 1, selectedAppartments[i]);
            }

            return dictionary;
        }

        private AppartementType GetAppartementType()
        {
            var dictionary = new Dictionary<int, AppartementType>();

            var values = Enum.GetValues(typeof(AppartementType));
            for (int i = 0; i < values.Length; i++)
            {
                var value = values.GetValue(i);
                dictionary.Add(i + 1, (AppartementType)value);
            }

            Console.WriteLine(ManagerResorces.rs_AppartementInputTypesInfo);
            foreach (var item in dictionary)
            {
                Console.Write(" {0}:{1};", item.Key, AppartmentConverter.AppartementTypeToString(item.Value));
            }
            Console.WriteLine();

            var commandId = ConsoleFunnyFunctions.GetPersistentVarian(1, dictionary.Count + 1, ManagerResorces.rs_InputAppartementTypesMessage,
                ManagerResorces.rs_InputAppartementTypesIncorrectNumberValue, ManagerResorces.rs_InputAppartementTypesNullNumberValue,
                ManagerResorces.rs_InputAppartementTypesNumberOutOfBounds);

            AppartementType result;
            if (dictionary.TryGetValue(commandId, out result))
            {
                return result;
            }
            return AppartementType.Economy;
        }

        private Appartement FillAppartementData()
        {
            var result = new Appartement();

            Console.Write(ManagerResorces.rs_InputAppartementRoomNumber);
            result.Number = Console.ReadLine();
            Console.Write(ManagerResorces.rs_InputAppartementDescription);
            result.Description = Console.ReadLine();

            result.Type = GetAppartementType();
            return result;
        }

        #endregion

        public Manager()
        {
            _bookingService = new BookingServiceFactory().GetService();

        }
        
        public void Wellcome()
        {
            Console.WriteLine(ManagerResorces.rs_ManagerWellcome);
        }

        public void Menu()
        {
            try
            {


                for (current = CheckYesAnswer; CheckYesAnswer == current;)
                {
                    try
                    {
                        Console.WriteLine("{0}\n{1}\n", ManagerResorces.rs_Select, ManagerResorces.rs_Menu);
                        // P.S.: Собственно, тут я пишу бесконечную попытку считать правильный вариант выбора, используется что бы не копипасстить код.
                        int selectMenu = ConsoleFunnyFunctions.GetPersistentVarian(1, 6, string.Empty,
                            ManagerResorces.rs_IncorectVariantData, ManagerResorces.rs_VariantIsEmptyError,
                            ManagerResorces.rs_VariantOutOfBounds);

                        switch (selectMenu)
                        {
                            case 1:
                                ShowAllAppartmentsCommand();
                                break;
                            case 2:
                                WriteHotelInfo();
                                break;
                            case 3:
                                ShowAllFreeAppsForDateCommand();
                                break;
                            case 4:
                                ShowCheckInDesckForPeriod();
                                break;
                            case 5:
                                SaveDataCommand();
                                break;
                            case 6:
                                ManagerMenueCommandCommand();

                                break;
                        }

                    }
                    catch (Exception)
                    {
                        Console.WriteLine("{0}\n{1}", ManagerResorces.rs_InvalidInput, ManagerResorces.rs_SelectDisplay); current = Console.ReadLine();
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine(ManagerResorces.rs_SomethingWrong);
                return;
            }
        }
    }
}
