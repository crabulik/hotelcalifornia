﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCommon
{
    public static class ConsoleFunnyFunctions
    {

        public static double GetPersistentDouble(string firstOffer, string incorrectOffer, string emptyOffer)
        {
            var separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var secondSeparator = ".";
            if (separator == secondSeparator)
                secondSeparator = ",";

            Console.Write(firstOffer);
            while (true)
            {

                var value = Console.ReadLine();
                if (!string.IsNullOrEmpty(value))
                {
                    value = value.Replace(secondSeparator, separator);
                    double attribute;
                    if (double.TryParse(value, out attribute))
                        return attribute;

                    Console.Write(incorrectOffer);
                }
                else
                {
                    Console.Write(emptyOffer);
                }
            }
        }

        public static float GetPersistentFloat(string firstOffer, string incorrectOffer, string emptyOffer)
        {
            var separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var secondSeparator = ".";
            if (separator == secondSeparator)
                secondSeparator = ",";

            Console.Write(firstOffer);
            while (true)
            {

                var value = Console.ReadLine();
                if (!string.IsNullOrEmpty(value))
                {
                    value = value.Replace(secondSeparator, separator);
                    float attribute;
                    if (float.TryParse(value, out attribute))
                        return attribute;

                    Console.Write(incorrectOffer);
                }
                else
                {
                    Console.Write(emptyOffer);
                }
            }
        }

        public static float? GetPersistentFloatWithSkip(string firstOffer, string incorrectOffer)
        {
            var separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var secondSeparator = ".";
            if (separator == secondSeparator)
                secondSeparator = ",";

            Console.Write(firstOffer);
            while (true)
            {

                var value = Console.ReadLine();
                if (!string.IsNullOrEmpty(value))
                {
                    value = value.Replace(secondSeparator, separator);
                    float attribute;
                    if (float.TryParse(value, out attribute))
                        return attribute;

                    Console.Write(incorrectOffer);
                }
                else
                {
                    return null;
                }
            }
        }

        public static int GetPersistentInt(string firstOffer, string incorrectOffer, string emptyOffer)
        {
            var separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var secondSeparator = ".";
            if (separator == secondSeparator)
                secondSeparator = ",";

            Console.Write(firstOffer);
            while (true)
            {

                var value = Console.ReadLine();
                if (!string.IsNullOrEmpty(value))
                {
                    value = value.Replace(secondSeparator, separator);
                    int attribute;
                    if (int.TryParse(value, out attribute))
                        return attribute;

                    Console.Write(incorrectOffer);
                }
                else
                {
                    Console.Write(emptyOffer);
                }
            }
        }

        public static DateTime ReadPersistentDate(String firstOffer, string errorDateFormatOffer, string cultureName)
        {
            CultureInfo ciEnterCulture;
            DateTime dateValue;

            ciEnterCulture = CultureInfo.GetCultureInfo(cultureName);
            while (true)
            {
                Console.WriteLine(firstOffer);
                var dateInput = Console.ReadLine();
                //var result = DateTime.TryParse(dateInput, CultureInfo.InstalledUICulture, DateTimeStyles.AllowWhiteSpaces, out dateValue);
                var result = DateTime.TryParse(dateInput, ciEnterCulture, DateTimeStyles.AllowWhiteSpaces, out dateValue);
                if (result)
                {
                    break;
                }
                else
                {
                    Console.WriteLine(errorDateFormatOffer);

                }
            }
            return dateValue;
        }

        public static int GetPersistentVarian(int minVariant, int maxVariant, string firstOffer, string incorrectOffer, string emptyOffer, string outOfBoundsOffer)
        {
            if (minVariant > maxVariant)
                return 0;


            Console.Write(firstOffer);
            while (true)
            {

                var value = Console.ReadLine();
                if (!string.IsNullOrEmpty(value))
                {
                    int attribute;
                    if (int.TryParse(value, out attribute))
                    {
                        if ((attribute >= minVariant) && (attribute <= maxVariant))
                        {

                            return attribute;
                        }
                        else
                        {
                            Console.Write(outOfBoundsOffer);
                        }

                    }
                    else
                    {
                        Console.Write(incorrectOffer);
                    }
                }
                else
                {
                    Console.Write(emptyOffer);
                }
            }
        }
    }
}
